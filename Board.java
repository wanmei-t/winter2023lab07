public class Board
{
	 //field(s)
	 private Square[][] tictactoeBoard;
	 
	//constructor
	public Board()
	 {
		 this.tictactoeBoard = new Square[3][3];
		 resetBoard(); 
	 }
	 
	 //standard method(s)
	 public String toString()
	 {
		 String s = "  0 1 2\n";
		 for (int i = 0; i < this.tictactoeBoard.length; i++)
		 {
			 s += i + " ";
			 for (int j = 0; j < this.tictactoeBoard[i].length; j++)
			 {
				 s += this.tictactoeBoard[i][j] + " ";
			 }
			 s += "\n";
		 }
		 //substring to remove the last \n at the end of string s
		 return s.substring(0, s.length()-1);
	 }
	 
	 //no getters or setters
	 
	 //instance methods
	 public void resetBoard()
	 {		
		 for (int i = 0; i < this.tictactoeBoard.length; i++)
		 {
			 for (int j = 0; j < this.tictactoeBoard[i].length; j++)
			 {
				 this.tictactoeBoard[i][j] = Square.BLANK;
			 }
		 }
	 }
	 
	 public boolean placeToken(int row, int col, Square playerToken)
	 {
		 if (row >= 0 && row <= 2 && col >= 0 && col <= 2)
		 {
			 if (this.tictactoeBoard[row][col] == Square.BLANK)
			 {
				 this.tictactoeBoard[row][col] = playerToken;
				 return true;
			 }
			 else
			 {
				 System.out.println("There is already a token at row " + row + " and column " + col);
				 return false;
			 }
		 }
		 else
		 {
			System.out.println("Number(s) should be between 0 and 2");
			return false;
		 }
	 }
	 
	 public boolean checkIfFull()
	 {
		 for (int i = 0; i < this.tictactoeBoard.length; i++)
		 {
			 for (Square n : tictactoeBoard[i])
			 {
				 if (n == Square.BLANK) return false;				 
			 }
		 }		 
		 return true;
	 }
	 
	 public boolean checkIfWinning(Square playerToken)
	 {
		return (checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) || checkIfWinningDiagonal(playerToken));
	 }
	 
	 //private helper methods
	 private boolean checkIfWinningHorizontal(Square playerToken)
	 {
		 for (int i = 0; i < this.tictactoeBoard.length; i++)
		 {
			 int count = 0;
			 for (int j = 0; j < this.tictactoeBoard[i].length; j++)
			 {
				 if (this.tictactoeBoard[i][j] == playerToken) count++;
			 }
			 if (count == 3) return true;
		 }
		 return false;
	 }
	 
	 private boolean checkIfWinningVertical(Square playerToken)
	 {
		//I hard coded this.tictactoeBoard[0].length into the for loop's second statement, because the length is always 3
		for (int col = 0; col < this.tictactoeBoard[0].length; col++)
		{
			int count = 0;
			for (int i = 0; i < this.tictactoeBoard.length; i++)
			{
				if (this.tictactoeBoard[i][col] == playerToken) count++;
			}
			if (count == 3) return true;
		}
		return false;
	 }
	 
	 private boolean checkIfWinningDiagonal(Square playerToken)
	 {
		if (this.tictactoeBoard[1][1] == playerToken)
		{
			if (this.tictactoeBoard[0][0] == playerToken && this.tictactoeBoard[2][2] == playerToken) return true;				
			else if (this.tictactoeBoard[0][2] == playerToken && this.tictactoeBoard[2][0] == playerToken) return true;
			else return false;
		}
		return false;
	 }
}