import java.util.Scanner;
public class TicTacToeGame
{	
	private static Scanner scan = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		//welcome message
		System.out.println("Welcome to Tic Tac Toe!");
		System.out.println("Player 1's token: X\tPlayer 2's token: O");
		
		//setup		
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Square playerToken = Square.X;
		int row, col;
		int countP1 = 0;
		int countP2 = 0;
		
		//the game
		while (!gameOver)
		{
			System.out.println(board);
			System.out.println("Player " + player + ", where do you want to place your token?");
			
			if (player == 1)
			{
				playerToken = Square.X;
			}
			else
			{
				playerToken = Square.O;
			}
						
			do {				
				System.out.println("Enter a row between 0 and 2, then enter a column between 0 and 2:");				
				row = scan.nextInt();
				col = scan.nextInt();					
			} while (!board.placeToken(row, col, playerToken));
			
			if (board.checkIfFull())
			{
				System.out.println("It's a tie");
				gameOver = askToEndGame(board, countP1, countP2);				
			}
			else if (board.checkIfWinning(playerToken))
			{
				System.out.println();
				System.out.println("Player " + player + " is the winner");				
				if (player == 1) countP1++;
				else countP2++;	
				gameOver = askToEndGame(board, countP1, countP2);
			}
			else
			{
				player++;
				if (player > 2) player = 1;
			}							
		}		
		scan.close();
	}
	
	//bonus
	public static boolean askToEndGame(Board board, int countP1, int countP2)
	{
		System.out.println("Would you like to play another game?");
		System.out.println("Answer by 'yes' or 'no'");		
		scan.nextLine();
		char answer = scan.nextLine().charAt(0);		
		if (answer == 'n')
		{
			System.out.println("Player 1 has a total of " + countP1 + " wins and Player 2 has a total of " + countP2 + " wins.");
		}
		else board.resetBoard();
		return (answer == 'n');
	}
}